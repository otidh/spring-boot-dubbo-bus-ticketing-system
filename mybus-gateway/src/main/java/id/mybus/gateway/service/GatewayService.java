package id.mybus.gateway.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface GatewayService {

	public Object handle(HttpServletRequest request, HttpServletResponse response, String sysCode, String version, String interfaceCode, Map<String, Object> body);

	public Object handle2(HttpServletRequest request, HttpServletResponse response, String sysCode, String version, String interfaceCode, Map<String, Object> body);

}


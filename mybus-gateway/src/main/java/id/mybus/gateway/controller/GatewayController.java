/*
 * Copyright (C) 2011-2014 ShenZhen iBOXPAY Information Technology Co.,Ltd.
 * 
 * All right reserved.
 * 
 * This software is the confidential and proprietary
 * information of iBoxPay Company of China. 
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement 
 * you entered into with iBoxpay inc.
 *
 */

package id.mybus.gateway.controller;

import java.util.Map;
import java.util.concurrent.Callable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.WebAsyncTask;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 * The class GatewayController.
 *
 * Description: 网关请求控制器
 *
 * @author: nieminjie
 * @since: 2017年5月5日	
 * @version: 2017-05-05 18:02:19 nieminjie
 *
 */
@RestController
public class GatewayController {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 默认超时时间
	 */
	private final long DEFAULT_TIMEOUT = 30000l;

	@Autowired
	private GatewayService gatewayService;

	@Autowired
	private ThreadPoolTaskExecutor threadPoolTaskExecutor;

	@RequestMapping(value = "/{sysCode}/{version}/{interfaceCode}.json",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE )
	public Object json(@PathVariable String sysCode, @PathVariable String version, @PathVariable String interfaceCode, @RequestBody(required = false) Map<String, Object>body, HttpServletRequest request, HttpServletResponse response) {
		final String requestId = MDC.get(MDCFilter.REQUEST_ID);

		// 回调任务
		Callable<Object> callable = new Callable<Object>() {
			public Object call() throws Exception {
				MDC.put(MDCFilter.REQUEST_ID, requestId);
				try {
					return gatewayService.handle(request, response, sysCode, version, interfaceCode, body);
				} catch (Exception e) {
					return ResponseUtil.dealWithException(e);
				}
			}
		};

		// 任务超时时间
		Long timeout = AsyncTaskParam.getTimeout();
		if(null == timeout) {
			timeout = DEFAULT_TIMEOUT;
		}

		WebAsyncTask<Object> webAsyncTask = new WebAsyncTask<Object>(timeout, threadPoolTaskExecutor, callable);
		webAsyncTask.onTimeout(new Callable<Object>() {

			@Override
			public Object call() throws Exception {
				return ResponseUtil.dealWithException(new BizException(Biz.BIZ_BUSY)); //disable i have no time
				//return webAsyncTask;
			}

		});
		return webAsyncTask;
	}

	@RequestMapping(value = "/{sysCode}/{version}/{interfaceCode}.json",method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE )
	public Object multipart(@PathVariable String sysCode, @PathVariable String version, @PathVariable String interfaceCode, @RequestParam(required = false) Map<String, Object> body, MultipartHttpServletRequest request, HttpServletResponse response) {
		final String requestId = MDC.get(MDCFilter.REQUEST_ID);

		// 回调任务
		Callable<Object> callable = new Callable<Object>() {
			public Object call() throws Exception {
				MDC.put(MDCFilter.REQUEST_ID, requestId);
				try {
					return gatewayService.handle(request, response, sysCode, version, interfaceCode, body);
				} catch (Exception e) {
					return ResponseUtil.dealWithException(e);
				}
			}
		};

		// 任务超时时间
		Long timeout = AsyncTaskParam.getTimeout();
		if(null == timeout) {
			timeout = DEFAULT_TIMEOUT;
		}

		WebAsyncTask<Object> webAsyncTask = new WebAsyncTask<Object>(timeout, threadPoolTaskExecutor, callable);
		webAsyncTask.onTimeout(new Callable<Object>() {

			@Override
			public Object call() throws Exception {
				MDC.put(MDCFilter.REQUEST_ID, requestId);
				return ResponseUtil.dealWithException(new BizException(Biz.BIZ_BUSY));
			}

		});
		return webAsyncTask;
	}

	@RequestMapping(value = "/{sysCode}/{version}/{interfaceCode}.json",method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public Object urlencoded(@PathVariable String sysCode, @PathVariable String version, @PathVariable String interfaceCode, @RequestParam(required = false) Map<String, Object>body, HttpServletRequest request, HttpServletResponse response) {
		final String requestId = MDC.get(MDCFilter.REQUEST_ID);

		// 回调任务
		Callable<Object> callable = new Callable<Object>() {
			public Object call() throws Exception {
				MDC.put(MDCFilter.REQUEST_ID, requestId);
				try {
					return gatewayService.handle2(request, response, sysCode, version, interfaceCode, body);
				} catch (Exception e) {
					return ResponseUtil.dealWithException(e);
				}
			}
		};

		// 任务超时时间
		Long timeout = AsyncTaskParam.getTimeout();
		if(null == timeout) {
			timeout = DEFAULT_TIMEOUT;
		}

		WebAsyncTask<Object> webAsyncTask = new WebAsyncTask<Object>(timeout, threadPoolTaskExecutor, callable);
		webAsyncTask.onTimeout(new Callable<Object>() {

			@Override
			public Object call() throws Exception {
				return ResponseUtil.dealWithException(new BizException(Biz.BIZ_BUSY)); //disable i have no time
				//return webAsyncTask;
			}

		});
		return webAsyncTask;
	}

	@RequestMapping(value = "/healthCheck.json", produces = MediaType.TEXT_PLAIN_VALUE)
	public String healthCheck(HttpServletRequest request, HttpServletResponse response) {
		return "";
	}

}


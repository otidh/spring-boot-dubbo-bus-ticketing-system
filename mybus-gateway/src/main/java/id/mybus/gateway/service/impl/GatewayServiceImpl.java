package id.mybus.gateway.service.impl;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import id.mybus.gateway.service.GatewayService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GatewayServiceImpl implements GatewayService {
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private static final String TOKEN = "4a4fc70014c711eabcfb000c29a6d2b0";

	@Autowired
	private InterfaceConfigService interfaceConfigService;

	@Autowired
	private InterfaceHandlerSelector interfaceHandlerSelector;

	@Override
	public Object handle(HttpServletRequest request, HttpServletResponse response, String sysCode, String version, String interfaceCode, Map<String, Object> body) throws ErrorCodeException {

		// 1ã€�ç­›é€‰æŽ¥å�£
		InterfaceConfig config = interfaceConfigService.select(sysCode, version, interfaceCode);

		// 2ã€�ç­›é€‰å¤„ç�†ç±»
		InterfaceHandler handler = null;
		if(null != config) {
			handler = interfaceHandlerSelector.getHandler(config);
		}
		if(null == handler) {
			throw new BizException(Biz.NOT_SUPPORTED_REQUEST);
		}
		
		// Add check waba app token
		if (config.getType().intValue() == 1) {
			String appToken = request.getHeader("APP_TOKEN");
			if (!WABA_TOKEN.equals(appToken)) {
				throw new BizException(Biz.NOT_SUPPORTED_REQUEST);
			}
		}

		try {

			// XSSæ³¨å…¥æ”»å‡»è¿‡æ»¤
			if(null != body && !body.isEmpty()) {
				body = JsonUtil.parseJSON2Map(Jsoup.clean(JsonUtil.toJson(body), Whitelist.relaxed()));				
			}
		} catch (IOException e) {
			throw new BizRuntimeException(e);
		}
		
		return handler.handle(request, response, config, body);

	}

	@Override
	public Object handle2(HttpServletRequest request, HttpServletResponse response, String sysCode, String version, String interfaceCode, Map<String, Object> body) throws ErrorCodeException {

		// 1ã€�ç­›é€‰æŽ¥å�£
		InterfaceConfig config = interfaceConfigService.select(sysCode, version, interfaceCode);

		// 2ã€�ç­›é€‰å¤„ç�†ç±»
		InterfaceHandler handler = null;
		if(null != config) {
			handler = interfaceHandlerSelector.getHandler(config);
		}
		if(null == handler) {
			throw new BizException(Biz.NOT_SUPPORTED_REQUEST);
		}

		return handler.handle(request, response, config, body);

	}

}


package id.mybus.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.filter.OrderedCharacterEncodingFilter;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author muhammaddhitoprihardhanto
 * created on 09/07/20 17.08
 */
@SpringBootApplication
@EnableAsync
//EnableAspectJAutoProxy(proxyTargetClass = true)
@ComponentScan(basePackages={"id.mybus.gateway"})
public class Application {

	/*@Bean
	public FilterRegistrationBean characterEncodingFilterRegistration() {
		OrderedCharacterEncodingFilter characterEncodingFilter = new OrderedCharacterEncodingFilter();
		characterEncodingFilter.setEncoding("UTF-8");
		characterEncodingFilter.setForceEncoding(true);

		FilterRegistrationBean registration = new FilterRegistrationBean(characterEncodingFilter);
		registration.setName("characterEncodingFilter");
		registration.setAsyncSupported(true);
		registration.addUrlPatterns("/*");
		return registration;
	}*/

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}

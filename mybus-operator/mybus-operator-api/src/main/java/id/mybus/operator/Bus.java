package id.mybus.operator;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;


/**
 * @author muhammaddhitoprihardhanto
 * created on 09/07/20 15.05
 */
@Getter
@Setter
public class Bus implements Serializable {
	private static final long serialVersionUID = -966840957618084503L;

	private Integer id;
	private String name;
	private String from;
	private String to;
	private Integer capacity;

}

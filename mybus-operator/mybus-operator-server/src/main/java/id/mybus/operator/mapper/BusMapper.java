package id.mybus.operator.mapper;

import id.mybus.operator.Bus;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface BusMapper {

	@Select("SELECT * FROM BUS WHERE id = #{id}")
	Bus findById(@Param("id") Integer id);

}

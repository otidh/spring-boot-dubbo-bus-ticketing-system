package id.mybus.operator.api;

import id.mybus.operator.Bus;

/**
 * @author muhammaddhitoprihardhanto
 * created on 09/07/20 15.43
 */
public interface BusService {

	Bus getById(Integer id);
}
